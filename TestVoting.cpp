// ---------------
// TestVoting.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Voting.hpp"

using namespace std;


// ----
// eval
// ----

TEST(VotingFixture, eval0) {
    candidate candidates[MAX_NUM_CANDIDATES];
    candidates[0].name = "John Doe";
    candidates[1].name = "Jane Smith";
    candidates[2].name = "Sirhan Sirhan";
    candidates[3].name = "Taka Koutso";
    int test[3][4] = {{0, 1, 2, 3},{1, 2, 3, 0},{2, 3, 0, 1}};
    ballot ballots[3];
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            ballots[i].prefs[j] = test[i][j];
        }
    }
    candidates[0].addBallot(&ballots[0]);
    candidates[1].addBallot(&ballots[1]);
    candidates[2].addBallot(&ballots[2]);
    string result = voting_eval(candidates, 3, 4);
    ASSERT_EQ(result, "John Doe\nJane Smith\nSirhan Sirhan");
}

TEST(VotingFixture, eval1) {
    candidate candidates[MAX_NUM_CANDIDATES];
    candidates[0].name = "John Doe";
    candidates[1].name = "Jane Smith";
    candidates[2].name = "Sirhan Sirhan";
    candidates[3].name = "Taka Koutso";
    int test[4][4] = {{0, 1, 2, 3},{1, 2, 3, 0},{2, 3, 0, 1}, {3, 0, 1, 2}};
    ballot ballots[4];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            ballots[i].prefs[j] = test[i][j];
        }
    }
    candidates[0].addBallot(&ballots[0]);
    candidates[1].addBallot(&ballots[1]);
    candidates[2].addBallot(&ballots[2]);
    candidates[3].addBallot(&ballots[3]);
    string result = voting_eval(candidates, 4, 4);
    ASSERT_EQ(result, "John Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso");
}

TEST(VotingFixture, eval2) {
    candidate candidates[MAX_NUM_CANDIDATES];
    candidates[0].name = "John Doe";
    candidates[1].name = "Jane Smith";
    candidates[2].name = "Sirhan Sirhan";
    int test[5][3] = {{0, 1, 2},{1, 0, 2},{1, 2, 0},{0, 1, 2},{2, 0, 1}};
    ballot ballots[5];
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 3; j++) {
            ballots[i].prefs[j] = test[i][j];
        }
    }
    candidates[0].addBallot(&ballots[0]);
    candidates[1].addBallot(&ballots[1]);
    candidates[1].addBallot(&ballots[2]);
    candidates[0].addBallot(&ballots[3]);
    candidates[2].addBallot(&ballots[4]);
    string result = voting_eval(candidates, 3, 4);
    ASSERT_EQ(result, "John Doe");
}

TEST(VotingFixture, eval3) {
    candidate candidates[MAX_NUM_CANDIDATES];
    candidates[0].name = "John Doe";
    candidates[1].name = "Jane Smith";
    candidates[2].name = "Sirhan Sirhan";
    int test[1][3] = {{0, 1, 2}};
    ballot ballots[1];
    for (int i = 0; i < 1; i++) {
        for (int j = 0; j < 3; j++) {
            ballots[i].prefs[j] = test[i][j];
        }
    }
    candidates[0].addBallot(&ballots[0]);
    string result = voting_eval(candidates, 3, 1);
    ASSERT_EQ(result, "John Doe");
}

TEST(VotingFixture, eval4) {
    candidate candidates[MAX_NUM_CANDIDATES];
    candidates[0].name = "John Doe";
    candidates[1].name = "Jane Smith";
    candidates[2].name = "Sirhan Sirhan";
    int test[6][3] = {{0, 1, 2},{1, 0, 2},{1, 2, 0},{0, 1, 2},{2, 0, 1},{2, 1, 0}};
    ballot ballots[6];
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 3; j++) {
            ballots[i].prefs[j] = test[i][j];
        }
    }
    candidates[0].addBallot(&ballots[0]);
    candidates[1].addBallot(&ballots[1]);
    candidates[1].addBallot(&ballots[2]);
    candidates[0].addBallot(&ballots[3]);
    candidates[2].addBallot(&ballots[4]);
    candidates[2].addBallot(&ballots[5]);
    string result = voting_eval(candidates, 6, 3);
    ASSERT_EQ(result, "John Doe\nJane Smith\nSirhan Sirhan");
}

TEST(VotingFixture, eval5) {
    candidate candidates[MAX_NUM_CANDIDATES];
    candidates[0].name = "John Doe";
    candidates[1].name = "Jane Smith";
    candidates[2].name = "Sirhan Sirhan";
    candidates[3].name = "Taka Koutso1";
    candidates[4].name = "Taka Koutso2";
    int test[6][5] = {{0, 1, 2, 3, 4},{1, 0, 2, 3, 4},{1, 2, 0, 3, 4},{0, 1, 2, 3, 4},{2, 0, 1, 3, 4},{2, 1, 0, 3, 4}};
    ballot ballots[6];
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 5; j++) {
            ballots[i].prefs[j] = test[i][j];
        }
    }
    candidates[0].addBallot(&ballots[0]);
    candidates[1].addBallot(&ballots[1]);
    candidates[1].addBallot(&ballots[2]);
    candidates[0].addBallot(&ballots[3]);
    candidates[2].addBallot(&ballots[4]);
    candidates[2].addBallot(&ballots[5]);
    string result = voting_eval(candidates, 6, 3);
    ASSERT_EQ(result, "John Doe\nJane Smith\nSirhan Sirhan");
}



// -----
// print
// -----

TEST(VotingFixture, print0) {
    ostringstream sout;
    voting_print(sout, string("test"), 0);
    ASSERT_EQ(sout.str(), "test\n");
}

TEST(VotingFixture, print1) {
    ostringstream sout;
    voting_print(sout, string("test"), 1);
    ASSERT_EQ(sout.str(), "\ntest\n");
}

TEST(VotingFixture, print2) {
    ostringstream sout;
    voting_print(sout, string("candidate 1\ncandidate 2"), 1);
    ASSERT_EQ(sout.str(), "\ncandidate 1\ncandidate 2\n");
}

TEST(VotingFixture, print3) {
    ostringstream sout;
    voting_print(sout, string("candidate 1\ncandidate 2\ncandidate 3"), 0);
    ASSERT_EQ(sout.str(), "candidate 1\ncandidate 2\ncandidate 3\n");
}

TEST(VotingFixture, print4) {
    ostringstream sout;
    voting_print(sout, string("candidate 1"), 0);
    ASSERT_EQ(sout.str(), "candidate 1\n");
}

// -----
// solve
// -----

TEST(VotingFixture, solve0) {
    istringstream sin("1\n\n3\nJohn Doe\nJane Smith\nSirhan Sirhan\n1 2 3\n2 1 3\n2 3 1\n1 2 3\n3 1 2");
    ostringstream sout;
    voting_solve(sin, sout);
    ASSERT_EQ(sout.str(), "John Doe\n");
}

TEST(VotingFixture, solve1) {
    istringstream sin("2\n\n3\nJohn Doe\nJane Smith\nSirhan Sirhan\n1 2 3\n2 1 3\n2 3 1\n1 2 3\n3 1 2\n\n3\nJohn Doe\nJane Smith\nSirhan Sirhan\n3 2 1\n3 1 2\n1 3 2\n3 2 1\n2 1 3");
    ostringstream sout;
    voting_solve(sin, sout);
    ASSERT_EQ(sout.str(), "John Doe\n\nSirhan Sirhan\n");
}

TEST(VotingFixture, solve2) {
    istringstream sin("1\n\n8\nWe're\nGonna\nTake\nAs\nMany\nRounds\nAs\nPossible\n1 2 3 4 5 6 7 8\n2 3 4 5 6 7 8 1\n3 4 5 6 7 8 1 2\n4 5 6 7 8 1 2 3\n5 6 7 8 1 2 3 4\n6 7 8 1 2 3 4 \n7 8 1 2 3 4 5 6\n8 1 2 3 4 5 6 7\n1 2 7 3 6 8 4 5\n");
    ostringstream sout;
    voting_solve(sin, sout);
    ASSERT_EQ(sout.str(), "We're\n");

}

TEST(VotingFixture, solve3) {
    istringstream sin("1\n\n4\nJohn Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n1 2 3 4\n 2 3 4 1\n 3 4 1 2");
    ostringstream sout;
    voting_solve(sin, sout);
    ASSERT_EQ(sout.str(), "John Doe\nJane Smith\nSirhan Sirhan\n");
}

TEST(VotingFixture, solve4) {
    istringstream sin("1\n\n4\nJohn Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n1 2 3 4\n 2 3 4 1\n 3 4 1 2\n4 1 2 3");
    ostringstream sout;
    voting_solve(sin, sout);
    ASSERT_EQ(sout.str(), "John Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n");
}

TEST(VotingFixture, solve5) {
    istringstream sin("2\n\n4\nJohn Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n1 2 3 4\n 2 3 4 1\n 3 4 1 2\n4 1 2 3\n\n4\nJohn Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n1 2 3 4\n 2 3 4 1\n 3 4 1 2");
    ostringstream sout;
    voting_solve(sin, sout);
    ASSERT_EQ(sout.str(), "John Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n\nJohn Doe\nJane Smith\nSirhan Sirhan\n");
}

TEST(VotingFixture, solve6) {
    istringstream sin("1\n\n4\nJohn Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n1 2 3 4");
    ostringstream sout;
    voting_solve(sin, sout);
    ASSERT_EQ(sout.str(), "John Doe\n");
}

TEST(VotingFixture, solve7) {
    istringstream sin("1\n\n4\nJohn Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n1 2 3 4\n3 2 1 4\n4 2 3 1\n1 2 3 4\n3 2 1 4\n4 2 3 1\n2 1 3 4");
    ostringstream sout;
    voting_solve(sin, sout);
    ASSERT_EQ(sout.str(), "John Doe\n");
}

TEST(VotingFixture, solve8) {
    istringstream sin("2\n\n4\nJohn Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n1 2 3 4\n3 2 1 4\n4 2 3 1\n1 2 3 4\n3 2 1 4\n4 2 3 1\n\n4\nJohn Doe\nJane Smith\nSirhan Sirhan\nTaka Koutso\n1 2 3 4\n3 2 1 4\n4 2 3 1\n1 2 3 4\n3 2 1 4\n4 2 3 1\n2 1 3 4");
    ostringstream sout;
    voting_solve(sin, sout);
    ASSERT_EQ(sout.str(), "John Doe\nSirhan Sirhan\nTaka Koutso\n\nJohn Doe\n");
}

