// ---------
// Voting.h
// ---------

#ifndef Voting_h
#define Voting_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <tuple>    // tuple
#include <utility>  // pair

// --------
// defines
// --------

#define MAX_NUM_CANDIDATES 20  /*!< Max number of candidates */
#define MAX_NUM_BALLOTS 1000   /*!< Max number of ballots */

using namespace std;

// --------
// ballot -- class to represent a ballot
// --------

class ballot {
public:

    /**
     * array containing preferences of a ballot
     */
    int prefs[MAX_NUM_CANDIDATES];

    /**
     * Default ballot constructor
     */
    ballot();

    /**
     * @param b existing array preferences array
     * Secondary ballot constructor
     */
    ballot(int b[MAX_NUM_CANDIDATES]);

};

// --------
// ballot -- class to represent a ballot
// --------
class candidate {
public:

    /**
     * C++ string to contain the candidate's name
     */
    string name;

    /**
     * array of ballot objects to represent ballots voting for this candidate
     */
    ballot ballots[MAX_NUM_BALLOTS];

    /**
     * gives current number of votes/size of ballots array data
     */
    int numVotes = 0;

    /**
     * default candidate constructor
     */
    candidate();

    /**
     * @param n C++ string for candidate name
     * Secondary candidate constructor
     */
    candidate(string n);

    /**
     * @param b pointer to ballot to copy over
     * adds current ballot to list of ballots voting for this candidate
     */
    void addBallot(ballot* b);

    /**
     * @param prefs existing array preferences
     * adds current ballot to list of ballots voting for this candidate
     */
    void addBallot(int prefs[MAX_NUM_CANDIDATES]);
};


// ------------
// voting_eval
// ------------

/**
 * @param candidates array of candidates in the election
 * @param numBallots number of ballots submitted
 * @param numCandidates number of candidates in the election
 * @return string corresponding the winner(s) of the election
 */
string voting_eval (candidate candidates[MAX_NUM_CANDIDATES], int numBallots, int numCandidates);

// -------------
// voting_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param tcase number representing which test case this is
 */
void voting_print (ostream& sout, string s, int tcase);

// -------------
// voting_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void voting_solve (istream&, ostream&);


#endif // Voting_h
