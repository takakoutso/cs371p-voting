// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Voting.hpp"

using namespace std;

// --------
// ballot
// --------

ballot::ballot() {

    for (int i = 0; i < MAX_NUM_CANDIDATES; i++) {
        prefs[i] = 0;
    }

}

ballot::ballot(int b[MAX_NUM_CANDIDATES]) {
    assert(b != nullptr);

    for (int i = 0; i < MAX_NUM_CANDIDATES; i++) {
        prefs[i] = b[i];
    }
}

// --------
// candidate
// --------

candidate::candidate() {

    numVotes = 0;

}

candidate::candidate(string n) {

    name = n;
    numVotes = 0;
}

void candidate::addBallot(ballot* b) {

    assert(b != nullptr);
    for (int i = 0; i < MAX_NUM_CANDIDATES; i++) {
        ballots[numVotes].prefs[i] = b->prefs[i];
    }
    numVotes++;
}

void candidate::addBallot(int prefs[MAX_NUM_CANDIDATES]) {
    assert (prefs != nullptr);
    for (int i = 0; i < MAX_NUM_CANDIDATES; i++) {
        ballots[numVotes].prefs[i] = prefs[i];
    }
    numVotes++;
}


// ------------
// voting_eval
// ------------

string voting_eval (candidate candidates[MAX_NUM_CANDIDATES], int numBallots, int numCandidates) {

    assert(candidates != nullptr);
    assert(numBallots > 0 && numBallots <= MAX_NUM_BALLOTS);
    assert(numCandidates > 0 && numCandidates <= MAX_NUM_CANDIDATES);



    bool eliminated[MAX_NUM_CANDIDATES] = {0};

    int toWin = numBallots / 2 + 1;
    for (int cand = 0; cand < MAX_NUM_CANDIDATES; cand++) { // check to see if first preferences are enough to declare a winner
        if (candidates[cand].numVotes >= toWin) {
            return candidates[cand].name;
        }
    }

    for (int round = 0; round < numCandidates; round++) {   // loop for each "round" of the election, max amount of rounds is numCandidates - 1

        int minInd = 0;
        while (eliminated[minInd]) {  // get a valid index to start looking for lowest votes with
            minInd++;
        }

        for (int cand = 0; cand < numCandidates; cand++) { // get index of lowest number of votes
            if (candidates[cand].numVotes < candidates[minInd].numVotes && !eliminated[cand]) {
                minInd = cand;
            }
        }

        bool isTie = true;
        for (int cand = 0; cand < numCandidates; cand++) { // if all votes are the minimum, we have a tie
            if (candidates[cand].numVotes != candidates[minInd].numVotes && !eliminated[cand]) {
                isTie = false;
                break;
            }
        }

        if (isTie) {
            string result("");  // C++ string to hold all candidates names
            for (int cand = 0; cand < numCandidates; cand++) { // append all tied candidates to the string
                if (!eliminated[cand]) {
                    if (isTie) {        // isTie now becomes a way to check if we are the first candidate or not
                        result += candidates[cand].name;    // if first candidate, just append the name
                        isTie = false;
                    } else {
                        result += "\n";                     // if not the first tied candidate, append a \n too
                        result += candidates[cand].name;
                    }
                }
            }
            return result;  // early return with candidates' names
        }

        int minAmount = candidates[minInd].numVotes;
        bool toBeEliminated[MAX_NUM_CANDIDATES] = {0};

        for (int cand = 0; cand < numCandidates; cand++) { // we will remove all the candidates with the lowest number of votes
            if (candidates[cand].numVotes == minAmount) {
                toBeEliminated[cand] = true;
            }
        }


        for (int cand = 0; cand < numCandidates; cand++) { // actually remove the marked candidates
            if (toBeEliminated[cand]) {
                candidate *c = &candidates[cand];
                for (int ball = 0; ball < c->numVotes; ball++) { // look at all assigned ballots

                    ballot *b = &(c->ballots[ball]);
                    int prefInd = 0;
                    while (eliminated[b->prefs[prefInd]] || toBeEliminated[b->prefs[prefInd]]) { // look for the first valid candidate on this ballot
                        prefInd++;
                    }
                    int preference = b->prefs[prefInd];
                    candidates[preference].addBallot(b);
                    if (candidates[preference].numVotes >= toWin) { // if this ballot tipped a candidate over the edge, they win.
                        return candidates[preference].name;
                    }
                }
                eliminated[cand] = true; // fully eliminate this candidate
            }
        }


    }

    assert(true); // assertion that we ran the election properly and never reached this point

    return candidates[0].name; // default return - will never run in a valid election
}

// -------------
// voting_print
// -------------

void voting_print (ostream& sout, string s, int tcase) {
    if (tcase > 0) {
        sout << endl;
    }
    sout << s << endl;
}

// -------------
// voting_solve
// -------------

void voting_solve (istream& sin, ostream& sout) {
    string s;

    int t;
    sin >> t;

    for (int tcase = 0; tcase < t; tcase++) { // loop for each test case

        int n;
        sin >> n;
        getline(sin, s);  // after we have the number of candidates, flush the line

        candidate candidates[MAX_NUM_CANDIDATES]; // array to hold all data

        for (int j = 0; j < n; j++) {

            getline(sin, s); // record each candidates' name
            candidates[j].name = s;

        }

        int ballotNum = 0;
        while (getline(sin, s)) { // while we have data to read, keep going

            if (s == "") { // if we reached the end of the data, stop
                break;
            }

            istringstream str(s);
            int vote;

            int prefs[MAX_NUM_CANDIDATES];

            for (int pref = 0; pref < n; pref++) { // read all preferences, convert to zero indexed and store
                str >> vote;
                vote--;
                prefs[pref] = vote;
            }

            candidates[prefs[0]].addBallot(prefs); // add the current ballot

            ballotNum++;

        }

        voting_print(sout, voting_eval(candidates, ballotNum, n), tcase); // compute this election cycle and print out the winning candidates

    }

}
