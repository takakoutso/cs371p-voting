# CS371p: Object-Oriented Programming Voting Repo

* Name: Vasilis (Taka) Koutsomitopoulos

* EID: vtk97

* GitLab ID: takakoutso

* HackerRank ID: taka_koutso

* Git SHA: d175fffc2628b0aea786e2ee4ba465c8a142605c

* GitLab Pipelines: https://gitlab.com/takakoutso/cs371p-voting/-/pipelines

* Estimated completion time: 6.0 

* Actual completion time: 5.0

* Comments: gcov was causing my pipeline to fail, i think bc of gitignore. Also coverage was lowered 30% by assertions